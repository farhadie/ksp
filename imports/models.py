# *-* encoding: utf-8 *-*
from __future__ import unicode_literals

from django.db import models


class FileImport(models.Model):
    ifile = models.FileField(upload_to='imported')
    name = models.CharField(max_length=50)
    when = models.DateTimeField(auto_now_add=True)
    warnings = models.TextField(default='', blank=True)

    def __str__(self):
        return self.ifile.name
