# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FileImport',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ifile', models.FileField(upload_to='imported')),
                ('when', models.DateTimeField(auto_now_add=True)),
                ('warnings', models.TextField(default='')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
