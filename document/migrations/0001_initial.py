# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('doc', models.FileField(upload_to='documents', verbose_name='File')),
                ('title', models.CharField(max_length=255, verbose_name='Title')),
                ('date', models.DateField(verbose_name='Date')),
                ('weight', models.SmallIntegerField(default=0)),
            ],
        ),
    ]
