from django.contrib.gis.db import models


class WaldausscheidungLaufend(models.Model):
    datum_import = models.DateTimeField()
    geom = models.PolygonField(srid=2056)

    class Meta:
        db_table = '"fp"."waldausscheidung_laufend"'
        managed = False
