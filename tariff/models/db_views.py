from django.db import models

from gemeinde.models import Gemeinde
from observation.models.models import (
    AdminRegion, Plot, PlotObs, TreeSpecies, Tree
)


class Basis_ddom:
    plotobs = models.OneToOneField(PlotObs, primary_key=True, verbose_name="OBS ID", db_column='id',
                                   on_delete=models.DO_NOTHING)
    ddom = models.FloatField()

    class Meta:
        db_table = "basis_ddom"


class Basis_gwl:
    plot = models.ForeignKey(db_column='id', primary_key=True, on_delete=models.deletion.DO_NOTHING,
                             to='observation.Plot')
    year = models.ForeignKey(db_column='year', on_delete=models.deletion.DO_NOTHING,
                             to='observation.PlotObs')
    region = models.ForeignKey(PlotObs, db_column='region', on_delete=models.deletion.DO_NOTHING,
                               serialize=False)
    acidity = models.ForeignKey(PlotObs, db_column='acidity', on_delete=models.deletion.DO_NOTHING,
                                serialize=False)
    exposition = models.ForeignKey(Plot, db_column='exposition', on_delete=models.deletion.DO_NOTHING,
                                   serialize=False)
    relief = models.ForeignKey(PlotObs, db_column='relief', on_delete=models.deletion.DO_NOTHING,
                               serialize=False)
    sealevel = models.ForeignKey(Plot, db_column='sealevel', on_delete=models.deletion.DO_NOTHING,
                                 serialize=False)
    gwl = models.FloatField()

    class Meta:
        db_table = "basis_gwl"


class Basis_volume:
    treeobs = models.ForeignKey(db_column='id', primary_key=True, on_delete=models.deletion.DO_NOTHING,
                       serialize=False, to='observation.TreeObs')

    tree = models.ForeignKey(db_column='id', on_delete=models.deletion.DO_NOTHING,
                               serialize=False, to='observation.Tree')
    treespec = models.ForeignKey(db_column='spec', on_delete=models.deletion.DO_NOTHING,
                                  serialize=False, to='observation.Tree')
    region = models.ForeignKey(db_column='region', on_delete=models.deletion.DO_NOTHING,
                                 serialize=False, to='observation.PlotObs')
    gwl = models.FloatField()
    ddom = models.FloatField()
    bifurcation = models.FloatField()
    sealevel = models.FloatField()
    storey = models.FloatField()
    volume = models.FloatField()

    class Meta:
        db_table = "basis_volume"