
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tariff', '0001_initial'),
    ]

    operations = [
        # gwl function: parameters: region, acidity, geology, exposition, relief and sealevel of the plot
        migrations.RunSQL("""CREATE OR REPLACE FUNCTION public.ksp_tariff_gwl(region integer, acidity integer, geology integer, exposition character varying, relief integer, elevation integer)
  RETURNS double precision AS
$BODY$DECLARE fc_tmp double precision;
DECLARE gwl double precision;
BEGIN
	SELECT k/(elevation-c)+m+(p*elevation) INTO fc_tmp
	FROM factor_combination INNER JOIN total_increment ON (factor_combination.fc = total_increment.fc)
	WHERE  region_id = region AND acidity_id = acidity AND (exposition_id = exposition) AND (geology_id = geology OR (geology_id IS NULL AND geology IS NULL)) AND relief_id = relief;

	return fc_tmp;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;;""", "DROP FUNCTION ksp_tariff_gwl(integer, integer, integer, character varying, integer, integer)"),

        # Ddom function: return Ddom of the plot. parameter: plot_id and edgef of the plot_obs
        migrations.RunSQL("""CREATE OR REPLACE FUNCTION public.ksp_tariff_ddom(plot integer, edgef numeric)
  RETURNS numeric AS
$BODY$
DECLARE ddom numeric;
BEGIN
  WITH temp AS (
    SELECT tree_id, dbh, vita_id, number
      FROM tree_obs JOIN numbers ON (10000/(9.77^2)*3.14)*edgef >= Numbers.number
      WHERE obs_id = plot
      ORDER BY dbh DESC
      ), largest_trees AS (
        SELECT * FROM temp LIMIT 100
      )
    SELECT AVG(dbh) into ddom
    FROM largest_trees;
    RETURN ddom;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;""", "DROP FUNCTION ksp_tariff_ddom(integer, numeric)"),

        # sealevel function: returns the sealevel of the tree. parameter: tree_obs ID
        migrations.RunSQL("""CREATE OR REPLACE FUNCTION public.ksp_tariff_sea_level(tree integer)
  RETURNS smallint AS
$BODY$
DECLARE
    sl SMALLINT;
BEGIN
  SELECT sealevel INTO sl
  FROM plot RIGHT JOIN plot_obs ON plot_obs.plot_id = plot.id LEFT JOIN tree_obs ON tree_obs.obs_id = plot_obs.id
  WHERE tree = tree_obs.id;
  RETURN sl;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;""", "DROP FUNCTION ksp_tariff_sea_level(integer)"),

        # returns storey of the tree, parameter: tree_obs ID
        migrations.RunSQL("""CREATE OR REPLACE FUNCTION public.ksp_tariff_storey(tree integer)
  RETURNS integer AS
$BODY$DECLARE
  storey integer;
BEGIN
  SELECT rank_id INto storey
  FROM tree_obs
  WHERE tree_obs.id = tree;

  IF storey < 3 THEN
  RETURN 1;
  ELSE
  RETURN 0;
  END IF;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;""", "DROP FUNCTION ksp_tariff_storey(integer)"),

        # returns the needed coefficients for the tree, based on region and specie of the tree
        migrations.RunSQL("""CREATE OR REPLACE FUNCTION public.ksp_tree_coefficients(region integer, spec integer)
  RETURNS nfi_coefficients AS
$BODY$
  SELECT nfi_coefficients.id, nfi_coefficients.tariff_number, b0,b1,b2,b3,b4,b5,b6,b7
  FROM nfi_coefficients
    LEFT JOIN tariff_number ON tariff_number.tariff_number= nfi_coefficients.tariff_number
  WHERE region = tariff_number.region_id AND spec = tariff_number.spec_id;
$BODY$
  LANGUAGE sql VOLATILE
  COST 100;""", "DROP FUNCTION ksp_tree_coefficients(integer, integer)"),

        # Bifurcation function: generate bifurcation parameter: 9.95% of trees are bifurcated
        migrations.RunSQL("""CREATE OR REPLACE FUNCTION public.ksp_tariff_bifurcation_of_stem()
  RETURNS integer AS
$BODY$
BEGIN
  IF random() <0.0995 THEN
    RETURN 1;
  ELSE
    RETURN 0;
  END IF;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;""", "DROP FUNCTION ksp_tariff_bifurcation_of_stem()"),

        # NFI tariff function: get tree_obs ID as parameter and return predicted volume of the tree
        migrations.RunSQL("""CREATE OR REPLACE FUNCTION public.ksp_nfi_tariff_function(input_tree integer)
  RETURNS numeric AS
$BODY$DECLARE
  spec integer;
  b0 numeric;
  b1 numeric;
  b2 numeric;
  b3 numeric;
  b4 numeric;
  b5 numeric;
  b6 numeric;
  b7 numeric;
  region integer;
  volume numeric;
BEGIN
  select tree.spec_id, region_id into spec, region
  from tree_obs join tree on tree.id = tree_obs.tree_id join plot_obs on tree_obs.obs_id = plot_obs.id
  where tree_obs.id = input_tree;

  SELECT ksp_tree_coefficients.b0,
    ksp_tree_coefficients.b1,
    ksp_tree_coefficients.b2,
    ksp_tree_coefficients.b3,
    ksp_tree_coefficients.b4,
    ksp_tree_coefficients.b5,
    ksp_tree_coefficients.b6,
    ksp_tree_coefficients.b7
    INTO b0,b1,b2,b3,b4,b5,b6,b7
  FROM ksp_tree_coefficients(region, spec);

  SELECT exp(b0 + b1 *ln(dbh) + b2 * (ln(dbh)^4) + b3 * ksp_tariff_gwl(plot_obs.region_id,plot_obs.acidity_id, null, plot.exposition, plot_obs.relief_id, plot.sealevel)+ b4 * ksp_tariff_ddom(plot.id, plot_obs.forest_edgef)+ b5 * ksp_tariff_bifurcation_of_stem()+ b6 * ksp_tariff_sea_level(tree_obs.id)+ b7 * ksp_tariff_storey(tree_obs.id)) INTO volume
  from tree_obs join plot_obs on tree_obs.obs_id = plot_obs.id join plot on plot_obs.plot_id = plot.id
  where tree_obs.id = input_tree;
  RETURN volume;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;""", "DROP FUNCTION public.ksp_nfi_tariff_function(integer)")
    ]
