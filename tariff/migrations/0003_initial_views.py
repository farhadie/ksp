import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ('tariff', '0001_initial'),
        ('tariff', '0002_tariff_functions'),
    ]

    operations = [
        migrations.CreateModel(
            name='basis_ddom',
            fields=[
                ('plotobs',
                 models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True,
                                   serialize=False, to='observation.PlotObs')),
                ('ddom', models.FloatField()),
            ],
            options={
                'db_table': 'basis_ddom',
                'managed': False,
            },
        ),
        migrations.RunSQL("""
        CREATE OR REPLACE VIEW public.basis_ddom AS
        SELECT plot_obs.plot_id,
          ksp_tariff_ddom(plot_obs.plot_id, plot_obs.forest_edgef) AS ddom
        FROM plot_obs
        ORDER BY plot_obs.plot_id;""",
                          "DROP VIEW basis_ddom"
                          ),
        migrations.CreateModel(
            name='basis_gwl',
            fields=[
                ('plot',
                 models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True,
                                   serialize=False, to='observation.Plot')),
                ('year', models.ForeignKey(db_column='year', on_delete=django.db.models.deletion.DO_NOTHING,
                                           serialize=False, to='observation.PlotObs')),
                ('region', models.ForeignKey(db_column='region', on_delete=django.db.models.deletion.DO_NOTHING,
                                             serialize=False, to='observation.PlotObs')),
                ('acidity', models.ForeignKey(db_column='acidity', on_delete=django.db.models.deletion.DO_NOTHING,
                                              serialize=False, to='observation.PlotObs')),
                ('exposition', models.ForeignKey(db_column='exposition', on_delete=django.db.models.deletion.DO_NOTHING,
                                                 serialize=False, to='observation.Plot')),
                ('relief', models.ForeignKey(db_column='relief', on_delete=django.db.models.deletion.DO_NOTHING,
                                             serialize=False, to='observation.PlotObs')),
                ('sealevel', models.ForeignKey(db_column='region', on_delete=django.db.models.deletion.DO_NOTHING,
                                               serialize=False, to='observation.Plot')),
                ('gwl', models.FloatField()),
            ],
            options={
                'db_table': 'basis_gwl',
                'managed': False,
            },
        ),
        migrations.RunSQL("""
        CREATE OR REPLACE VIEW public.basis_gwl AS
         SELECT plot.id,
            plot_obs.plot_id,
            plot_obs.year,
            plot_obs.region_id,
            plot_obs.acidity_id,
            plot_obs.geology_id,
            plot.exposition,
            plot_obs.relief_id,
            plot.sealevel,
            ksp_tariff_gwl(plot_obs.region_id, plot_obs.acidity_id, plot_obs.geology_id, plot.exposition, plot_obs.relief_id, plot.sealevel::integer) AS gwl
           FROM plot_obs
             FULL JOIN plot ON plot.id = plot_obs.plot_id;""",
                          "DROP VIEW basis_gwl"
                          ),
        migrations.CreateModel(
            name='basis_volume',
            fields=[
                ('treeobs',
                 models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True,
                                   serialize=False, to='observation.TreeObs')),

                ('tree', models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING,
                                           serialize=False, to='observation.Tree')),
                ('treespec', models.ForeignKey(db_column='spec', on_delete=django.db.models.deletion.DO_NOTHING,
                                               serialize=False, to='observation.Tree')),
                ('region', models.ForeignKey(db_column='region', on_delete=django.db.models.deletion.DO_NOTHING,
                                             serialize=False, to='observation.PlotObs')),
                ('gwl', models.FloatField()),
                ('ddom', models.FloatField()),
                ('bifurcation', models.FloatField()),
                ('sealevel', models.FloatField()),
                ('storey', models.FloatField()),
                ('volume', models.FloatField()),
            ],
            options={
                'db_table': 'basis_volume',
                'managed': False,
            },
        ),
        migrations.RunSQL("""
    CREATE OR REPLACE VIEW public.basis_volume AS
     SELECT tree_obs.id,
        tree_obs.tree_id,
        tree.spec_id,
        plot_obs.region_id,
        ksp_tariff_gwl(plot_obs.region_id, plot_obs.acidity_id, NULL::integer, plot.exposition, plot_obs.relief_id, plot.sealevel::integer) AS gwl,
        ksp_tariff_ddom(plot_obs.id, plot_obs.forest_edgef) AS ddom,
        ksp_tariff_bifurcation_of_stem() AS bifurcation_of_stem,
        ksp_tariff_sea_level(tree_obs.id) AS sea_level,
        ksp_tariff_storey(tree_obs.id) AS storey,
        ksp_nfi_tariff_function(tree_obs.id) AS volume
       FROM tree_obs
         JOIN tree ON tree.id = tree_obs.tree_id
         JOIN plot_obs ON plot_obs.id = tree_obs.obs_id
         JOIN plot ON plot_obs.plot_id = plot.id
      WHERE tree_obs.dbh > 0;""",
                          "DROP VIEW basis_volume"
                          ),

    ]
