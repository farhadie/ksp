// Missing IE indexOf for arrays
if (!Array.prototype.indexOf) {
   Array.prototype.indexOf = function(item) {
      var i = this.length;
      while (i--) {
         if (this[i] === item) return i;
      }
      return -1;
   }
}

function color_standardfehler(elements) {
    // Set cell color when standard error greater than some threshold
    var greater_than_10 = elements.filter(function() {
        return parseInt($(this).text()) > 10;
    }).css("background-color", "#fabf8f");
    var greater_than_25 = greater_than_10.filter(function() {
        return parseInt($(this).text()) > 25;
    }).css("background-color", "#ff6161");
    var empty = elements.filter(function() {
        return ($(this).text() == '' && !isNaN(parseInt($(this).prev().text())));
    }).css("background-color", "#ff6161");
    greater_than_10.prev().css("background-color", "#fabf8f");
    greater_than_25.prev().css("background-color", "#ff6161");
    empty.prev().css("background-color", "#ff6161");
}

$(document).ready(function() {
    $('div.togglable').prepend('<img class="tri" src="' + static_url + 'img/tri-closed-white.png' + '">');
    $('div.togglable').click(function (ev) {
        var target = $(this).next();
        if (target.is(':visible')) {
            $('img.tri', this).attr('src', static_url + 'img/tri-closed-white.png');
        } else {
            $('img.tri', this).attr('src', static_url + 'img/tri-open-white.png');
        }
        target.toggle();
    });
    $('select[name="inventory_filter"]').change(function(ev) {
        window.location.href = '?aufn=' + $(this).val();
    });
    $(document).tooltip();
});

