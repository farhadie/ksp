# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = []

    operations = [
        migrations.RunSQL("""CREATE OR REPLACE FUNCTION ksp_lokale_dichte(edgef numeric) RETURNS numeric AS $$
SELECT CASE WHEN $1=1 THEN 100.0 / 3.0
            WHEN $1=0 THEN 0
            ELSE 100.0 / 3.0 * 1 / $1
       END;
$$ LANGUAGE SQL;""",
            "DROP FUNCTION ksp_lokale_dichte(edgef numeric)"),


        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION ksp_volume_ha(diameter smallint, dichte numeric) RETURNS numeric AS $$
  SELECT CASE WHEN $1 = 0 THEN 0 ELSE (($1 * $1 * 0.0011) - (@$1 * 0.0011) - 0.079) * $2 END AS volume_ha;
$$ LANGUAGE SQL;""",
            "DROP FUNCTION ksp_volume_ha(diameter smallint, dichte numeric)"),

        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION ksp_grundflaeche_bl(diameter smallint) RETURNS double precision AS $$
  SELECT pi() * $1 * $1 / 4 / 10000;
$$ LANGUAGE SQL IMMUTABLE;""",
            "DROP FUNCTION ksp_grundflaeche_bl(diameter smallint)"),

        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION ksp_grundflaeche_ha(diameter smallint, dichte numeric) RETURNS double precision AS $$
  SELECT pi() * $1 * $1 / 4 / 10000 * $2;
$$ LANGUAGE SQL IMMUTABLE;""",
            "DROP FUNCTION ksp_grundflaeche_ha(diameter smallint, dichte numeric)"),


        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION ksp_volume_et_bl(diameter smallint) RETURNS numeric AS $$
  SELECT CASE WHEN $1 = 0 THEN 0 ELSE (($1 * $1 * 0.0011) - (@$1 * 0.0011) - 0.079) END AS volume_baum;
$$
  LANGUAGE SQL IMMUTABLE""",
            "DROP FUNCTION ksp_volume_et_bl(smallint)"),
    ]
