import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('gemeinde', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Acidity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_acidity',
            },
        ),
        migrations.CreateModel(
            name='AdminRegion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nr', models.CharField(max_length=10, unique=True)),
                ('name', models.CharField(max_length=100)),
                ('geom', django.contrib.gis.db.models.fields.MultiPolygonField(blank=True, null=True, srid=2056)),
            ],
            options={
                'db_table': 'adminregion',
            },
        ),
        migrations.CreateModel(
            name='CrownClosure',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100, verbose_name="Beschreibung")),
            ],
            options={
                'db_table': 'lt_crown_closure',
                'verbose_name': "Schlussgrad",
            },
        ),
        migrations.CreateModel(
            name='CrownForm',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
                ('explanation', models.TextField(blank=True, default='')),
            ],
            options={
                'db_table': 'lt_crown_form',
            },
        ),
        migrations.CreateModel(
            name='CrownLength',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
                ('explanation', models.TextField(blank=True, default='')),
            ],
            options={
                'db_table': 'lt_crown_len',
            },
        ),
        migrations.CreateModel(
            name='Damage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_damage',
            },
        ),
        migrations.CreateModel(
            name='DamageCause',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_damage_cause',
            },
        ),
        migrations.CreateModel(
            name='DevelStage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100, verbose_name="Beschreibung")),
            ],
            options={
                'db_table': 'lt_devel_stage',
                'verbose_name': "Entwicklungsstufe",
            },
        ),
        migrations.CreateModel(
            name='ForestDomain',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'forest_domain',
            },
        ),
        migrations.CreateModel(
            name='ForestForm',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_forest_form',
            },
        ),
        migrations.CreateModel(
            name='ForestMixture',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100, verbose_name="Beschreibung")),
            ],
            options={
                'db_table': 'lt_forest_mixture',
                'verbose_name': "Mischungsgrad",
            },
        ),
        migrations.CreateModel(
            name='Gap',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_gap',
            },
        ),
        migrations.CreateModel(
            name='Geology',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_geology',
            },
        ),
        migrations.CreateModel(
            name='Layer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
                ('explanation', models.TextField(blank=True, default='')),
            ],
            options={
                'db_table': 'lt_layer',
            },
        ),
        migrations.CreateModel(
            name='Owner',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('typ', models.CharField(db_column='type', max_length=1)),
                ('forest', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='observation.ForestDomain')),
            ],
            options={
                'db_table': 'owner',
            },
        ),
        migrations.CreateModel(
            name='OwnerType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_owner_type',
            },
        ),
        migrations.CreateModel(
            name='Phytosoc',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=6)),
                ('description', models.CharField(max_length=200, verbose_name="Beschreibung")),
                ('inc_class', models.SmallIntegerField(verbose_name='Ertragsklasse')),
                ('ecol_grp', models.CharField(max_length=1, verbose_name='Ökologische Gruppe')),
                ('bwnatur', models.SmallIntegerField(blank=True, db_column='BWNATURN_WSL', null=True)),
            ],
            options={
                'db_table': 'phytosoc',
                'verbose_name': 'Phytosoziologie',
            },
        ),
        migrations.CreateModel(
            name='Plot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nr', models.PositiveIntegerField()),
                ('the_geom', django.contrib.gis.db.models.fields.PointField(srid=2056)),
                ('slope', models.SmallIntegerField(blank=True, null=True, verbose_name='Neigung')),
                ('exposition', models.CharField(blank=True, choices=[('N', 'Norden'), ('S', 'Süden')], default='', max_length=1)),
                ('sealevel', models.SmallIntegerField(verbose_name='Höhe über Meer (m)')),
                ('igis', models.SmallIntegerField(blank=True, null=True)),
                ('checked', models.BooleanField(default=False)),
                ('phytosoc', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.Phytosoc')),
            ],
            options={
                'db_table': 'plot',
            },
        ),
        migrations.CreateModel(
            name='PlotObs',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('year', models.SmallIntegerField(db_index=True, verbose_name='Aufnahmejahr')),
                ('inv_period', models.SmallIntegerField(null=True, verbose_name='Aufnahmeperiode')),
                ('area', models.SmallIntegerField(blank=True, null=True)),
                ('subsector', models.CharField(blank=True, max_length=2)),
                ('evaluation_unit', models.CharField(blank=True, max_length=4)),
                ('forest_clearing', models.BooleanField(default=False)),
                ('stand', models.SmallIntegerField(blank=True, null=True)),
                ('forest_edgef', models.DecimalField(decimal_places=1, max_digits=2, verbose_name='Waldrandfaktor')),
                ('gwl', models.FloatField(verbose_name='Gesamtwuchsleistung (gwl)')),
                ('remarks', models.TextField(blank=True, verbose_name='Bermerkungen')),
                ('iprobenr', models.SmallIntegerField()),
                ('ipsi1', models.IntegerField()),
                ('ipsi2', models.IntegerField()),
                ('ipsf1', models.DecimalField(decimal_places=6, max_digits=10)),
                ('ipsf2', models.DecimalField(decimal_places=6, max_digits=10)),
                ('iflae', models.DecimalField(decimal_places=6, max_digits=10)),
                ('itflae', models.DecimalField(decimal_places=6, max_digits=10)),
                ('ikalter', models.IntegerField()),
                ('ik6', models.IntegerField(blank=True, null=True)),
                ('ik7', models.IntegerField(blank=True, null=True)),
                ('ik8', models.IntegerField(blank=True, null=True)),
                ('ik9', models.IntegerField(blank=True, null=True)),
                ('ik10', models.IntegerField(blank=True, null=True)),
                ('izeitpkt', models.CharField(blank=True, max_length=1)),
                ('abt', models.CharField(blank=True, max_length=10)),
                ('ikflag', models.IntegerField()),
                ('ifoa', models.CharField(blank=True, max_length=5)),
                ('irevf', models.IntegerField(blank=True, null=True)),
                ('idistr', models.CharField(blank=True, max_length=2)),
                ('iabt', models.CharField(blank=True, max_length=10)),
                ('iuabt', models.IntegerField(blank=True, null=True)),
                ('iufl', models.IntegerField(blank=True, null=True)),
                ('ibest', models.IntegerField(blank=True, null=True)),
                ('istao', models.CharField(blank=True, max_length=10)),
                ('ia', models.CharField(blank=True, max_length=1)),
                ('ib', models.IntegerField(blank=True, null=True)),
                ('ic', models.CharField(blank=True, max_length=1)),
                ('id_2', models.CharField(blank=True, max_length=5)),
                ('ie', models.CharField(blank=True, max_length=2)),
                ('isgr', models.DecimalField(decimal_places=6, max_digits=10)),
                ('ibgr', models.DecimalField(decimal_places=6, max_digits=10)),
                ('ibgr2', models.DecimalField(decimal_places=6, max_digits=10)),
                ('ibgr3', models.DecimalField(decimal_places=6, max_digits=10)),
                ('ibgr4', models.DecimalField(decimal_places=6, max_digits=10)),
                ('ibgr5', models.DecimalField(decimal_places=6, max_digits=10)),
                ('ibgr6', models.DecimalField(decimal_places=6, max_digits=10)),
                ('ibgr7', models.DecimalField(decimal_places=6, max_digits=10)),
                ('ibgr8', models.DecimalField(decimal_places=6, max_digits=10)),
                ('ibgr9', models.DecimalField(decimal_places=6, max_digits=10)),
            ],
            options={
                'db_table': 'plot_obs',
            },
        ),
        migrations.CreateModel(
            name='Rank',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
                ('explanation', models.TextField(blank=True, default='')),
            ],
            options={
                'db_table': 'lt_rank',
            },
        ),
        migrations.CreateModel(
            name='RegenObs',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('perc', models.DecimalField(decimal_places=2, max_digits=5)),
            ],
            options={
                'db_table': 'regen_obs',
            },
        ),
        migrations.CreateModel(
            name='RegenType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_regen_type',
            },
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_region',
            },
        ),
        migrations.CreateModel(
            name='RegionType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20)),
            ],
            options={
                'db_table': 'regiontype',
            },
        ),
        migrations.CreateModel(
            name='Relief',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_relief',
            },
        ),
        migrations.CreateModel(
            name='Sector',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_sector',
            },
        ),
        migrations.CreateModel(
            name='SoilCompaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_soil_compaction',
            },
        ),
        migrations.CreateModel(
            name='StandStructure',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_stand_struct',
            },
        ),
        migrations.CreateModel(
            name='Stem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_stem',
            },
        ),
        migrations.CreateModel(
            name='SurveyType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_survey_type',
            },
        ),
        migrations.CreateModel(
            name='Tree',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nr', models.PositiveSmallIntegerField(help_text='tree number')),
                ('azimuth', models.SmallIntegerField(help_text='angle in Grads')),
                ('distance', models.DecimalField(decimal_places=2, help_text='[in dm]', max_digits=6)),
                ('plot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='observation.Plot')),
            ],
            options={
                'db_table': 'tree',
            },
        ),
        migrations.CreateModel(
            name='TreeObs',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dbh', models.SmallIntegerField()),
                ('ianteil', models.DecimalField(blank=True, decimal_places=6, max_digits=10, null=True)),
                ('iverbiss', models.IntegerField()),
                ('iflag', models.IntegerField()),
                ('ih', models.DecimalField(blank=True, decimal_places=6, max_digits=8, null=True)),
                ('ibon', models.DecimalField(blank=True, decimal_places=6, max_digits=8, null=True)),
                ('ibanr', models.IntegerField()),
                ('ialter', models.IntegerField()),
                ('ibhd2', models.IntegerField()),
                ('ikreis', models.IntegerField(blank=True, null=True)),
                ('inadelv', models.IntegerField()),
                ('ivergilb', models.IntegerField()),
                ('ikron', models.DecimalField(blank=True, decimal_places=6, max_digits=8, null=True)),
                ('iintens2', models.CharField(blank=True, max_length=1)),
                ('itoth', models.IntegerField(blank=True, null=True)),
                ('izstam', models.CharField(blank=True, max_length=1)),
                ('ianzahl', models.DecimalField(blank=True, decimal_places=6, max_digits=10, null=True)),
                ('schicht', models.IntegerField()),
                ('iu', models.CharField(blank=True, max_length=20)),
                ('iv', models.IntegerField(blank=True, null=True)),
                ('iw', models.IntegerField(blank=True, null=True)),
                ('iz', models.IntegerField(blank=True, null=True)),
                ('crown_form', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.CrownForm')),
                ('crown_length', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.CrownLength')),
                ('damage', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.Damage')),
                ('damage_cause', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.DamageCause')),
                ('layer', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.Layer')),
            ],
            options={
                'db_table': 'tree_obs',
            },
        ),
        migrations.CreateModel(
            name='TreeSpecies',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('species', models.CharField(max_length=100)),
                ('code', models.SmallIntegerField(blank=True, help_text='National inventory value', null=True)),
                ('abbrev', models.CharField(max_length=4, unique=True)),
                ('is_tree', models.BooleanField(default=True, help_text='True if this is a real tree')),
                ('is_special', models.BooleanField(default=False)),
            ],
            options={
                'db_table': 'tree_spec',
            },
        ),
        migrations.CreateModel(
            name='UpdatedValue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('table_name', models.CharField(max_length=50)),
                ('row_id', models.IntegerField()),
                ('field_name', models.CharField(max_length=50)),
                ('old_value', models.CharField(max_length=255)),
                ('new_value', models.CharField(max_length=255)),
                ('stamp', models.DateTimeField(auto_now_add=True)),
                ('comment', models.TextField(blank=True, default='')),
            ],
            options={
                'db_table': 'updated_value',
            },
        ),
        migrations.CreateModel(
            name='Vita',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_vita',
            },
        ),
        migrations.AddField(
            model_name='treeobs',
            name='obs',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='observation.PlotObs'),
        ),
        migrations.AddField(
            model_name='treeobs',
            name='rank',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.Rank'),
        ),
        migrations.AddField(
            model_name='treeobs',
            name='stem',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.Stem'),
        ),
        migrations.AddField(
            model_name='treeobs',
            name='survey_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='observation.SurveyType'),
        ),
        migrations.AddField(
            model_name='treeobs',
            name='tree',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='observation.Tree'),
        ),
        migrations.AddField(
            model_name='treeobs',
            name='vita',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.Vita'),
        ),
        migrations.AddField(
            model_name='tree',
            name='spec',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='observation.TreeSpecies'),
        ),
        migrations.AddField(
            model_name='regenobs',
            name='obs',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='observation.PlotObs'),
        ),
        migrations.AddField(
            model_name='regenobs',
            name='spec',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='observation.TreeSpecies'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='acidity',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.Acidity', verbose_name='Azidität'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='crown_closure',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='observation.CrownClosure'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='forest_form',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.ForestForm', verbose_name='Waldform'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='forest_mixture',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='observation.ForestMixture'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='gap',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.Gap', verbose_name='Blösse'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='geology',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.Geology', verbose_name='Geologie'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='municipality',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='gemeinde.Gemeinde', verbose_name='Gemeinde'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='owner',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.Owner'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='owner_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.OwnerType', verbose_name='Besitzertyp'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='plot',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='observation.Plot', verbose_name='Aufnahmepunkt (plot)'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='regen_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.RegenType', verbose_name='Verjüngungsart'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='region',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.Region'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='relief',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.Relief'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='sector',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.Sector', verbose_name='Sektor'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='soil_compaction',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.SoilCompaction', verbose_name='Bodenverdichtung'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='stand_crown_closure',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='plotobs_stand', to='observation.CrownClosure', verbose_name='Schlussgrad'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='stand_devel_stage',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.DevelStage', verbose_name='Entwicklungsstufe'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='stand_forest_mixture',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='plotobs_stand', to='observation.ForestMixture', verbose_name='Mischungsgrad'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='stand_structure',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.StandStructure'),
        ),
        migrations.AddField(
            model_name='adminregion',
            name='region_type',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='observation.RegionType'),
        ),
        migrations.AlterUniqueTogether(
            name='treeobs',
            unique_together=set([('obs', 'tree')]),
        ),
        migrations.RunSQL(
            "ALTER TABLE plot_obs ADD CONSTRAINT forest_edgef_limits CHECK (forest_edgef BETWEEN 0 AND 1)",
            "DROP CONSTRAINT forest_edgef_limits"
        ),
        migrations.RunSQL(
            "COMMENT ON COLUMN plot_obs.forest_mixture_id IS 'Unused. "
            "See the field plot_obs.stand_forest_mixture_id instead.';"),
        migrations.RunSQL(
            "COMMENT ON COLUMN plot_obs.crown_closure_id IS 'Unused. "
            "See the field plot_obs.stand_crown_closure_id instead.';"),
    ]
