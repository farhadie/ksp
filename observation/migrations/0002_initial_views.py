import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('observation', '0001_initial'),
        ('observation', '0001_initial_functions'),
        ('observation', '0004_dead_wood_volume'),
    ]

    operations = [
        migrations.CreateModel(
            name='HolzProduktion',
            fields=[
                ('plotobs', models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='observation.PlotObs')),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'db_table': 'basis_grundf_vol_yx_pro_plotobs',
                'managed': False,
            },
        ),
        migrations.RunSQL("""
 CREATE OR REPLACE VIEW basis_grundf_vol_yx_pro_plotobs AS
 SELECT plot_obs.id,
    plot_obs.plot_id,
    plot_obs.id AS plot_obs_id,
    plot_obs.year,
    plot_obs.municipality_id,
    COALESCE(subq."Anzahl Probebaeume", 0) AS "Anzahl Probebaeume",
    COALESCE(subq."Volumen m3", 0) AS "Volumen m3",
    COALESCE(subq."Grundflaeche m2", 0) AS "Grundflaeche m2",
    COALESCE(subq."Anzahl Probebaeume"::numeric * ksp_lokale_dichte(plot_obs.forest_edgef), 0) AS "Stammzahl pro ha",
    COALESCE(subq."Volumen m3" * ksp_lokale_dichte(plot_obs.forest_edgef), 0) AS "Volumen pro ha",
    COALESCE(subq."Grundflaeche m2" * ksp_lokale_dichte(plot_obs.forest_edgef)::double precision, 0) AS "Grundflaeche pro ha",
    plot_obs.forest_edgef,
    plot_obs.inv_period
   FROM ( SELECT tree_obs.obs_id,
            count(*) AS "Anzahl Probebaeume",
            sum(ksp_volume_et_bl(tree_obs.dbh)) AS "Volumen m3",
            sum(ksp_grundflaeche_bl(tree_obs.dbh)) AS "Grundflaeche m2"
           FROM tree_obs
             JOIN tree ON tree_obs.tree_id = tree.id
          WHERE (tree_obs.vita_id = 2 OR tree_obs.vita_id = 3) AND tree_obs.dbh > 11
          GROUP BY tree_obs.obs_id) subq
     RIGHT JOIN plot_obs ON subq.obs_id = plot_obs.id;""",
            "DROP VIEW basis_grundf_vol_yx_pro_plotobs"
        ),

        migrations.CreateModel(
            name='EinwuchsProSpec',
            fields=[
                ('id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'db_table': 'basis_grundf_vol_yx_pro_plotobs_und_allspec_einwuchs',
                'managed': False,
            },
        ),
        migrations.RunSQL("""
 CREATE OR REPLACE VIEW basis_grundf_vol_yx_pro_plotobs_und_allspec_einwuchs AS
 SELECT (sub2.plot_obs_id || '-'::text) || sub2.tree_spec_id AS id,
    sub2.plot_id,
    sub2.plot_obs_id,
    sub2.year,
    sub2.tree_spec_id,
    sub2.municipality_id,
    COALESCE(subq."Anzahl Probebaeume", 0) AS "Anzahl Probebaeume",
    COALESCE(subq."Volumen m3", 0) AS "Volumen m3",
    COALESCE(subq."Grundflaeche m2", 0) AS "Grundflaeche m2",
    COALESCE(subq."Anzahl Probebaeume"::numeric * ksp_lokale_dichte(sub2.forest_edgef), 0) AS "Stammzahl pro ha",
    COALESCE(subq."Volumen m3" * ksp_lokale_dichte(sub2.forest_edgef), 0) AS "Volumen pro ha",
    COALESCE(subq."Grundflaeche m2" * ksp_lokale_dichte(sub2.forest_edgef)::double precision, 0) AS "Grundflaeche pro ha"
   FROM ( SELECT tree_obs.obs_id,
            tree.spec_id,
            count(*) AS "Anzahl Probebaeume",
            sum(ksp_volume_et_bl(tree_obs.dbh)) AS "Volumen m3",
            sum(ksp_grundflaeche_bl(tree_obs.dbh)) AS "Grundflaeche m2"
           FROM tree_obs
             JOIN tree ON tree_obs.tree_id = tree.id
          WHERE tree_obs.vita_id = 2 AND tree_obs.dbh > 11
          GROUP BY tree_obs.obs_id, tree.spec_id) subq
     RIGHT JOIN ( SELECT plot_obs.id AS plot_obs_id,
            plot_obs.plot_id,
            plot_obs.year,
            plot_obs.forest_edgef,
            plot_obs.municipality_id,
            tree_spec.id AS tree_spec_id
           FROM plot_obs,
            tree_spec) sub2 ON sub2.plot_obs_id = subq.obs_id AND sub2.tree_spec_id = subq.spec_id
  ORDER BY subq."Volumen m3" * ksp_lokale_dichte(sub2.forest_edgef);""",
            "DROP VIEW basis_grundf_vol_yx_pro_plotobs_und_allspec_einwuchs"
        ),

        migrations.CreateModel(
            name='HolzProduktionProSpec',
            fields=[
                ('id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'db_table': 'basis_grundf_vol_yx_pro_plotobs_und_allspec',
                'managed': False,
            },
        ),
        migrations.RunSQL("""
 CREATE OR REPLACE VIEW basis_grundf_vol_yx_pro_plotobs_und_allspec AS
 SELECT (sub2.plot_obs_id || '-'::text) || sub2.tree_spec_id AS id,
    sub2.plot_id,
    sub2.plot_obs_id,
    sub2.year,
    sub2.tree_spec_id,
    sub2.municipality_id,
    COALESCE(subq."Anzahl Probebaeume", 0) AS "Anzahl Probebaeume",
    COALESCE(subq."Volumen m3", 0) AS "Volumen m3",
    COALESCE(subq."Grundflaeche cm2", 0) AS "Grundflaeche cm2",
    COALESCE(subq."Anzahl Probebaeume"::numeric * ksp_lokale_dichte(sub2.forest_edgef), 0) AS "Stammzahl pro ha",
    COALESCE(subq."Volumen m3" * ksp_lokale_dichte(sub2.forest_edgef), 0) AS "Volumen pro ha",
    COALESCE(subq."Grundflaeche cm2" * ksp_lokale_dichte(sub2.forest_edgef)::double precision, 0) AS "Grundflaeche pro ha"
   FROM ( SELECT tree_obs.obs_id,
            tree.spec_id,
            count(*) AS "Anzahl Probebaeume",
            sum(ksp_volume_et_bl(tree_obs.dbh)) AS "Volumen m3",
            sum(ksp_grundflaeche_bl(tree_obs.dbh)) AS "Grundflaeche cm2"
           FROM tree_obs
             JOIN tree ON tree_obs.tree_id = tree.id
          WHERE (tree_obs.vita_id = 2 OR tree_obs.vita_id = 3) AND tree_obs.dbh > 11
          GROUP BY tree_obs.obs_id, tree.spec_id) subq
     RIGHT JOIN ( SELECT plot_obs.id AS plot_obs_id,
            plot_obs.plot_id,
            plot_obs.year,
            plot_obs.forest_edgef,
            plot_obs.municipality_id,
            tree_spec.id AS tree_spec_id
           FROM plot_obs,
            tree_spec) sub2 ON sub2.plot_obs_id = subq.obs_id AND sub2.tree_spec_id = subq.spec_id;""",
            "DROP VIEW basis_grundf_vol_yx_pro_plotobs_und_allspec"
        ),

        migrations.CreateModel(
            name='HomepageView',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('gemeinde', models.CharField(db_column='gemeindename', max_length=50)),
                ('jahr', models.SmallIntegerField(db_column='year', verbose_name='Aufnahmejahr')),
                ('probepunkte', models.IntegerField(db_column='Anzahl Probepunkte', verbose_name='Anzahl Probepunkte')),
                ('waldflaeche', models.DecimalField(db_column='theoretische Waldfläche ha', decimal_places=1, max_digits=5, verbose_name='theoretische Waldfläche pro ha')),
                ('probebaum_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('stammzahl_ha', models.IntegerField(db_column='Stammzahl pro ha', verbose_name='Stammzahl pro ha')),
                ('volumen_ha', models.DecimalField(db_column='Volumen pro ha', decimal_places=1, max_digits=5, verbose_name='Volumen [m3 pro ha]')),
                ('grundflaeche_ha', models.DecimalField(db_column='Grundflaeche pro ha', decimal_places=1, max_digits=5, verbose_name='Grundflaeche [m2 pro ha]')),
                ('inv_period', models.SmallIntegerField()),
            ],
            options={
                'db_table': 'web_homepage_holzproduktion',
                'managed': False,
            },
        ),
        migrations.RunSQL("""
 CREATE OR REPLACE VIEW web_homepage_holzproduktion AS
 SELECT row_number() OVER () AS id,
    d.gemeindename,
    b.inv_period,
    string_agg(DISTINCT b.year::text, '-'::text) AS year,
    sum(b."Anzahl Probebaeume")::integer AS "Anzahl Probebaeume",
    avg(b."Stammzahl pro ha")::integer AS "Stammzahl pro ha",
    stddev(b."Stammzahl pro ha")::double precision / sqrt(count(*)::double precision) / avg(b."Stammzahl pro ha")::double precision * 100::double precision AS "% Standardfehler",
    avg(b."Volumen pro ha")::numeric(5,1) AS "Volumen pro ha",
    stddev(b."Volumen pro ha")::double precision / sqrt(count(*)::double precision) / avg(b."Volumen pro ha")::double precision * 100::double precision AS "% Standardfehler2",
    avg(b."Grundflaeche pro ha")::numeric(5,1) AS "Grundflaeche pro ha",
    stddev(b."Grundflaeche pro ha") / sqrt(count(*)::double precision) / avg(b."Grundflaeche pro ha") * 100::double precision AS "% Standardfehler3",
    sum(ksp_lokale_dichte(b.forest_edgef) * 0.03 * 2.0)::numeric(5,1) AS "theoretische Waldfläche ha",
    count(*) AS "Anzahl Probepunkte"
   FROM basis_grundf_vol_yx_pro_plotobs b
     FULL JOIN gemeindegrenzen_bsbl d ON b.municipality_id = d.gid
  GROUP BY d.gemeindename, b.inv_period
  ORDER BY d.gemeindename, b.inv_period;""",
            "DROP VIEW web_homepage_holzproduktion"
        ),

        migrations.CreateModel(
            name='NutzungProSpec',
            fields=[
                ('id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'db_table': 'basis_grundf_vol_yx_pro_plotobs_und_allspec_nutzung',
                'managed': False,
            },
        ),
        migrations.RunSQL("""
 CREATE OR REPLACE VIEW basis_grundf_vol_yx_pro_plotobs_und_allspec_nutzung AS
 SELECT (sub2.plot_obs_id || '-'::text) || sub2.tree_spec_id AS id,
    sub2.plot_id,
    sub2.plot_obs_id,
    sub2.year,
    sub2.tree_spec_id,
    sub2.municipality_id,
    COALESCE(subq."Anzahl Probebaeume", 0) AS "Anzahl Probebaeume",
    COALESCE(subq."Volumen m3", 0) AS "Volumen m3",
    COALESCE(subq."Grundflaeche m2", 0) AS "Grundflaeche m2",
    COALESCE(subq."Anzahl Probebaeume"::numeric * ksp_lokale_dichte(sub2.forest_edgef), 0) AS "Stammzahl pro ha",
    COALESCE(subq."Volumen m3" * ksp_lokale_dichte(sub2.forest_edgef), 0) AS "Volumen pro ha",
    COALESCE(subq."Grundflaeche m2" * ksp_lokale_dichte(sub2.forest_edgef)::double precision, 0) AS "Grundflaeche pro ha"
   FROM ( SELECT tree_obs.obs_id,
            tree.spec_id,
            count(*) AS "Anzahl Probebaeume",
            sum(ksp_volume_et_bl(tree_obs.dbh)) AS "Volumen m3",
            sum(ksp_grundflaeche_bl(tree_obs.dbh)) AS "Grundflaeche m2"
           FROM tree_obs
             JOIN tree ON tree_obs.tree_id = tree.id
          WHERE tree_obs.vita_id = 1
          GROUP BY tree_obs.obs_id, tree.spec_id) subq
     RIGHT JOIN ( SELECT plot_obs.id AS plot_obs_id,
            plot_obs.plot_id,
            plot_obs.year,
            plot_obs.forest_edgef,
            plot_obs.municipality_id,
            tree_spec.id AS tree_spec_id
           FROM plot_obs,
            tree_spec) sub2 ON sub2.plot_obs_id = subq.obs_id AND sub2.tree_spec_id = subq.spec_id
  ORDER BY subq."Volumen m3" * ksp_lokale_dichte(sub2.forest_edgef);""",
            "DROP VIEW basis_grundf_vol_yx_pro_plotobs_und_allspec_nutzung"
        ),

        migrations.CreateModel(
            name='PlotInRegionView',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('plot', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='observation.Plot')),
                ('adminregion', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='observation.AdminRegion')),
                ('region_name', models.CharField(max_length=100)),
                ('region_type', models.CharField(max_length=20)),
            ],
            options={
                'db_table': 'plot_in_region',
                'managed': False,
            },
        ),
        migrations.RunSQL("""
 CREATE MATERIALIZED VIEW plot_in_region AS
 SELECT row_number() OVER ()::integer AS id,
    plot.id AS plot_id,
    adminregion.id AS adminregion_id,
    adminregion.name AS region_name,
    regiontype.name AS region_type
   FROM plot,
    adminregion
     LEFT JOIN regiontype ON adminregion.region_type_id = regiontype.id
  WHERE st_within(plot.the_geom, adminregion.geom)
WITH DATA;""",
            "DROP VIEW plot_in_region"
        ),

        migrations.CreateModel(
            name='TotholzProSpec',
            fields=[
                ('id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'db_table': 'basis_grundf_vol_yx_pro_plotobs_und_allspec_totholz',
                'managed': False,
            },
        ),
        migrations.RunSQL("""
 CREATE OR REPLACE VIEW basis_grundf_vol_yx_pro_plotobs_und_allspec_totholz AS
 SELECT (sub2.plot_obs_id || '-'::text) || sub2.tree_spec_id AS id,
    sub2.plot_id,
    sub2.plot_obs_id,
    sub2.year,
    sub2.tree_spec_id,
    sub2.municipality_id,
    COALESCE(subq."Anzahl Probebaeume", 0) AS "Anzahl Probebaeume",
    COALESCE(subq."Volumen m3", 0) AS "Volumen m3",
    COALESCE(subq."Grundflaeche m2", 0) AS "Grundflaeche m2",
    COALESCE(subq."Anzahl Probebaeume"::numeric * ksp_lokale_dichte(sub2.forest_edgef), 0) AS "Stammzahl pro ha",
    COALESCE(subq."Volumen m3" * ksp_lokale_dichte(sub2.forest_edgef), 0) AS "Volumen pro ha",
    COALESCE(subq."Grundflaeche m2" * ksp_lokale_dichte(sub2.forest_edgef)::double precision, 0) AS "Grundflaeche pro ha"
   FROM ( SELECT tree_obs.obs_id,
            tree.spec_id,
            count(*) AS "Anzahl Probebaeume",
            sum(ksp_volume_et_bl(tree_obs.dbh)) AS "Volumen m3",
            sum(ksp_grundflaeche_bl(tree_obs.dbh)) AS "Grundflaeche m2"
           FROM tree_obs
             JOIN tree ON tree_obs.tree_id = tree.id
          WHERE tree_obs.vita_id = 4
          GROUP BY tree_obs.obs_id, tree.spec_id) subq
     RIGHT JOIN ( SELECT plot_obs.id AS plot_obs_id,
            plot_obs.plot_id,
            plot_obs.year,
            plot_obs.forest_edgef,
            plot_obs.municipality_id,
            tree_spec.id AS tree_spec_id
           FROM plot_obs,
            tree_spec) sub2 ON sub2.plot_obs_id = subq.obs_id AND sub2.tree_spec_id = subq.spec_id
  ORDER BY subq."Volumen m3" * ksp_lokale_dichte(sub2.forest_edgef);"""
            "DROP VIEW basis_grundf_vol_yx_pro_plotobs_und_allspec_totholz"
        ),

        migrations.CreateModel(
            name='BWARTEN',
            fields=[
                ('plot_obs', models.OneToOneField(on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='observation.PlotObs', verbose_name='OBS ID')),
                ('bwarten', models.IntegerField(db_column='BWARTEN', verbose_name='Gehölzartenvielfalt (BWARTEN)')),
                ('num_spec', models.IntegerField(db_column='Anzahl Baumarten', verbose_name='Anzahl Baumarten')),
                ('special_spec', models.IntegerField(db_column='Special Species', verbose_name='Special Species')),
            ],
            options={
                'db_table': 'bwarten_anz_baumarten_special_species',
                'managed': False,
            },
        ),
        migrations.RunSQL("""CREATE OR REPLACE VIEW bwarten_anz_baumarten_special_species AS
 SELECT c.plot_obs_id,
    (
        CASE
            WHEN count(*) <= 1 THEN 1::bigint
            WHEN count(*) >= 5 THEN 5::bigint
            ELSE count(*)
        END + max(
        CASE
            WHEN c.is_special = true THEN 2
            ELSE 0
        END))::integer AS "BWARTEN",
    count(*) AS "Anzahl Baumarten",
    max(
        CASE
            WHEN c.is_special = true THEN 2
            ELSE 0
        END) AS "Special Species"
   FROM ( SELECT a.plot_obs_id,
            b.is_special,
            a.year,
            a.tree_spec_id,
            a."Anzahl Probebaeume"
           FROM basis_grundf_vol_yx_pro_plotobs_und_allspec a
             JOIN tree_spec b ON a.tree_spec_id = b.id
          WHERE a."Anzahl Probebaeume" > 0) c
  GROUP BY c.plot_obs_id;""",
            "DROP VIEW bwarten_anz_baumarten_special_species"
        ),

        migrations.CreateModel(
            name='BWNATURN',
            fields=[
                ('plot_obs', models.OneToOneField(on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='observation.PlotObs', verbose_name='OBS ID')),
                ('code', models.CharField(max_length=6)),
                ('bwnaturn', models.IntegerField(db_column='BWNATURN', verbose_name='Naturnähe des Nadelholzanteils (BWNATURN)')),
                ('fichte_perc', models.FloatField(db_column='%% Basalflächenanteil der Fichte', verbose_name='% Basalflächenanteil der Fichte')),
                ('nadel_perc', models.FloatField(db_column='%% Basalflächenanteil Nadelholz', verbose_name='% Basalflächenanteil Nadelholz')),
                ('nadel_wtanne_perc', models.FloatField(db_column='%% Basalflächenanteil Nadelholz ohne Tanne', verbose_name='% Basalflächenanteil Nadelholz ohne Tanne')),
            ],
            options={
                'db_table': 'bwnaturn_ndh_fi_ndh_ohne_ta',
                'managed': False,
            },
        ),
        migrations.RunSQL("""
 CREATE OR REPLACE VIEW basalflaechenanteil_fi_ndh AS
 SELECT a.plot_id,
    a.plot_obs_id,
    100.0::double precision / NULLIF(sum(a."Grundflaeche cm2"), 0) * sum(
        CASE
            WHEN a.tree_spec_id = 15 THEN a."Grundflaeche cm2"
            ELSE 0::double precision
        END) AS "% Basalflächenanteil der Fichte",
    100.0::double precision / NULLIF(sum(a."Grundflaeche cm2"), 0) * sum(
        CASE
            WHEN a.tree_spec_id = ANY (ARRAY[15, 16, 17, 18, 19, 20, 21]) THEN a."Grundflaeche cm2"
            ELSE 0::double precision
        END) AS "% Basalflächenanteil Nadelholz",
    100.0::double precision / NULLIF(sum(a."Grundflaeche cm2"), 0) * sum(
        CASE
            WHEN a.tree_spec_id = ANY (ARRAY[15, 17, 18, 19, 20, 21]) THEN a."Grundflaeche cm2"
            ELSE 0::double precision
        END) AS "% Basalflächenanteil Nadelholz ohne Tanne"
   FROM basis_grundf_vol_yx_pro_plotobs_und_allspec a
  GROUP BY a.plot_id, a.plot_obs_id;""",
            "DROP VIEW basalflaechenanteil_fi_ndh"
        ),
        migrations.RunSQL("""
 CREATE OR REPLACE VIEW bwnaturn_ndh_fi_ndh_ohne_ta AS
 SELECT b.plot_obs_id,
    b.plot_id,
    e.code,
        CASE
            WHEN b."% Basalflächenanteil der Fichte" > 75::double precision AND e."BWNATURN_WSL" <= 3 THEN 1
            WHEN b."% Basalflächenanteil Nadelholz" > 75::double precision AND b."% Basalflächenanteil der Fichte" < 75::double precision AND (e."BWNATURN_WSL" = 2 OR e."BWNATURN_WSL" = 3) THEN 2
            WHEN b."% Basalflächenanteil Nadelholz ohne Tanne" > 75::double precision AND b."% Basalflächenanteil der Fichte" < 75::double precision AND e."BWNATURN_WSL" = 1 THEN 2
            WHEN b."% Basalflächenanteil Nadelholz" <= 75::double precision AND b."% Basalflächenanteil Nadelholz" > 10::double precision AND e."BWNATURN_WSL" = 2 THEN 3
            WHEN b."% Basalflächenanteil Nadelholz" <= 75::double precision AND b."% Basalflächenanteil Nadelholz" > 25::double precision AND e."BWNATURN_WSL" = 3 THEN 3
            WHEN b."% Basalflächenanteil Nadelholz ohne Tanne" <= 75::double precision AND b."% Basalflächenanteil Nadelholz ohne Tanne" > 25::double precision AND e."BWNATURN_WSL" = 1 THEN 3
            WHEN b."% Basalflächenanteil Nadelholz" <= 10::double precision AND e."BWNATURN_WSL" = 2 THEN 4
            WHEN b."% Basalflächenanteil Nadelholz" <= 25::double precision AND e."BWNATURN_WSL" = 3 THEN 4
            WHEN b."% Basalflächenanteil Nadelholz ohne Tanne" <= 25::double precision AND e."BWNATURN_WSL" = 1 THEN 4
            WHEN e."BWNATURN_WSL" = 4 THEN 4
            ELSE NULL::integer
        END AS "BWNATURN",
    b."% Basalflächenanteil der Fichte",
    b."% Basalflächenanteil Nadelholz",
    b."% Basalflächenanteil Nadelholz ohne Tanne"
   FROM basalflaechenanteil_fi_ndh b
     JOIN plot_obs c ON b.plot_obs_id = c.id
     JOIN plot d ON d.id = c.plot_id
     LEFT JOIN phytosoc e ON e.id = d.phytosoc_id;""",
            "DROP VIEW bwnaturn_ndh_fi_ndh_ohne_ta"
        ),

        migrations.CreateModel(
            name='BWSTRU1M',
            fields=[
                ('id', models.OneToOneField(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='observation.PlotObs', verbose_name='OBS ID')),
                ('bwstru1m', models.IntegerField(db_column='BWSTRU1M', verbose_name='Strukturvielfalt (BWSTRU1M)')),
            ],
            options={
                'db_table': 'bwstru1m',
                'managed': False,
            },
        ),
        migrations.RunSQL("""
 CREATE OR REPLACE VIEW bwstru1m AS
 SELECT plot_obs.id,
    plot_obs.stand_devel_stage_id,
    plot_obs.stand_crown_closure_id,
    plot_obs.stand_structure_id,
        CASE
            WHEN plot_obs.stand_devel_stage_id = 1 THEN 1
            WHEN plot_obs.stand_devel_stage_id = 2 OR plot_obs.stand_devel_stage_id = 3 THEN 2
            WHEN plot_obs.stand_devel_stage_id = 4 THEN 2
            WHEN plot_obs.stand_devel_stage_id = 5 THEN 3
            WHEN plot_obs.stand_devel_stage_id = 6 THEN 6
            WHEN plot_obs.stand_devel_stage_id = 7 THEN 4
            ELSE NULL::integer
        END +
        CASE
            WHEN plot_obs.stand_crown_closure_id = 2 THEN 1
            WHEN plot_obs.stand_crown_closure_id = 3 OR plot_obs.stand_crown_closure_id = 4 THEN 3
            WHEN plot_obs.stand_crown_closure_id = 5 OR plot_obs.stand_crown_closure_id = 6 THEN 6
            WHEN plot_obs.stand_crown_closure_id = 7 THEN 5
            WHEN plot_obs.stand_crown_closure_id = 1 THEN 4
            ELSE NULL::integer
        END +
        CASE
            WHEN plot_obs.stand_structure_id = 1 THEN 1
            WHEN plot_obs.stand_structure_id = 2 THEN 3
            WHEN plot_obs.stand_structure_id = 3 OR plot_obs.stand_structure_id = 4 THEN 5
            ELSE NULL::integer
        END AS "BWSTRU1M"
   FROM plot_obs;""",
            "DROP VIEW bwstru1m"
        ),

        migrations.CreateModel(
            name='Biotopwert',
            fields=[
                ('id', models.OneToOneField(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='observation.PlotObs', verbose_name='OBS ID')),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('biolfi1m', models.FloatField(db_column='BIOLFI1M', verbose_name='Biotopwert (BIOLFI1M)')),
                ('bioklass', models.SmallIntegerField(db_column='Biotopwert Klassen', verbose_name='Biotopwert Klasse')),
            ],
            options={
                'db_table': 'Biotopwert Klassen pro plot_obs, year, owner_type_id',
                'managed': False,
            },
        ),
        migrations.RunSQL("""
 CREATE OR REPLACE VIEW biotopwert_bwnaturn_bwarten_bwstru1m AS
 SELECT d.id,
    d.plot_id,
    d.municipality_id,
    d.year,
    d.owner_type_id,
    b."BWNATURN"::double precision / 4::double precision + a."BWARTEN"::double precision / 7::double precision + 2::double precision * (c."BWSTRU1M"::double precision / 18::double precision) AS "BIOLFI1M",
    a."BWARTEN",
    b."BWNATURN",
    c."BWSTRU1M"
   FROM bwarten_anz_baumarten_special_species a
     JOIN bwnaturn_ndh_fi_ndh_ohne_ta b ON a.plot_obs_id = b.plot_obs_id
     JOIN bwstru1m c ON c.id = a.plot_obs_id
     JOIN plot_obs d ON d.id = a.plot_obs_id
  WHERE c."BWSTRU1M" IS NOT NULL;""",
            "DROP VIEW biotopwert_bwnaturn_bwarten_bwstru1m"
        ),
        migrations.RunSQL("""
 CREATE OR REPLACE VIEW "Biotopwert Klassen pro plot_obs, year, owner_type_id" AS
 SELECT b.id,
    b.municipality_id,
    b.plot_id,
    b.year,
    b.owner_type_id,
    b."BIOLFI1M",
        CASE
            WHEN b."BIOLFI1M" > 2.6::double precision THEN 4
            WHEN b."BIOLFI1M" > 2.0::double precision AND b."BIOLFI1M" <= 2.6::double precision THEN 3
            WHEN b."BIOLFI1M" > 1.6::double precision AND b."BIOLFI1M" <= 2.0::double precision THEN 2
            WHEN b."BIOLFI1M" <= 1.6::double precision THEN 1
            ELSE 1000
        END AS "Biotopwert Klassen"
   FROM biotopwert_bwnaturn_bwarten_bwstru1m b;""",
            'DROP VIEW "Biotopwert Klassen pro plot_obs, year, owner_type_id"'
        ),

        migrations.CreateModel(
            name='Einwuchs',
            fields=[
                ('plotobs', models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='observation.PlotObs')),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'db_table': 'basis_grundf_vol_yx_pro_plotobs_einwuchs',
                'managed': False,
            },
        ),
        migrations.RunSQL("""
CREATE OR REPLACE VIEW basis_grundf_vol_yx_pro_plotobs_einwuchs AS
 SELECT plot_obs.id,
    plot_obs.plot_id,
    plot_obs.id AS plot_obs_id,
    plot_obs.year,
    plot_obs.municipality_id,
    COALESCE(subq."Anzahl Probebaeume", 0) AS "Anzahl Probebaeume",
    COALESCE(subq."Volumen m3", 0) AS "Volumen m3",
    COALESCE(subq."Grundflaeche m2", 0) AS "Grundflaeche m2",
    COALESCE(subq."Anzahl Probebaeume"::numeric * ksp_lokale_dichte(plot_obs.forest_edgef), 0) AS "Stammzahl pro ha",
    COALESCE(subq."Volumen m3" * ksp_lokale_dichte(plot_obs.forest_edgef), 0) AS "Volumen pro ha",
    COALESCE(subq."Grundflaeche m2" * ksp_lokale_dichte(plot_obs.forest_edgef)::double precision, 0) AS "Grundflaeche pro ha",
    plot_obs.forest_edgef
   FROM ( SELECT tree_obs.obs_id,
            count(*) AS "Anzahl Probebaeume",
            sum(ksp_volume_et_bl(tree_obs.dbh)) AS "Volumen m3",
            sum(ksp_grundflaeche_bl(tree_obs.dbh)) AS "Grundflaeche m2"
           FROM tree_obs
             JOIN tree ON tree_obs.tree_id = tree.id
          WHERE tree_obs.vita_id = 2 AND tree_obs.dbh > 11
          GROUP BY tree_obs.obs_id) subq
     FULL JOIN plot_obs ON subq.obs_id = plot_obs.id;""",
            "DROP VIEW basis_grundf_vol_yx_pro_plotobs_einwuchs"
        ),

        migrations.CreateModel(
            name='Nutzung',
            fields=[
                ('plotobs', models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='observation.PlotObs')),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'db_table': 'basis_grundf_vol_yx_pro_plotobs_nutzung',
                'managed': False,
            },
        ),
        migrations.RunSQL("""
 CREATE OR REPLACE VIEW basis_grundf_vol_yx_pro_plotobs_nutzung AS
 SELECT plot_obs.id,
    plot_obs.plot_id,
    plot_obs.id AS plot_obs_id,
    plot_obs.year,
    plot_obs.municipality_id,
    COALESCE(subq."Anzahl Probebaeume", 0::bigint) AS "Anzahl Probebaeume",
    COALESCE(subq."Volumen m3", 0::numeric) AS "Volumen m3",
    COALESCE(subq."Grundflaeche m2", 0::double precision) AS "Grundflaeche m2",
    COALESCE(subq."Anzahl Probebaeume", 0::bigint)::numeric * ksp_lokale_dichte(plot_obs.forest_edgef) AS "Stammzahl pro ha",
    COALESCE(subq."Volumen m3", 0::numeric) * ksp_lokale_dichte(plot_obs.forest_edgef) AS "Volumen pro ha",
    COALESCE(subq."Grundflaeche m2", 0::double precision) * ksp_lokale_dichte(plot_obs.forest_edgef)::double precision AS "Grundflaeche pro ha",
    plot_obs.forest_edgef
   FROM ( SELECT tree_obs.obs_id,
            count(*) AS "Anzahl Probebaeume",
            sum(ksp_volume_et_bl(tree_obs.dbh)) AS "Volumen m3",
            sum(ksp_grundflaeche_bl(tree_obs.dbh)) AS "Grundflaeche m2"
           FROM tree_obs
             JOIN tree ON tree_obs.tree_id = tree.id
          WHERE tree_obs.vita_id = 1
          GROUP BY tree_obs.obs_id) subq
     FULL JOIN plot_obs ON subq.obs_id = plot_obs.id;""",
            "DROP VIEW basis_grundf_vol_yx_pro_plotobs_nutzung"
        ),

        migrations.CreateModel(
            name='Totholz',
            fields=[
                ('plotobs', models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='observation.PlotObs')),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'db_table': 'basis_grundf_vol_yx_pro_plotobs_totholz',
                'managed': False,
            },
        ),
        migrations.RunSQL("""
 CREATE OR REPLACE VIEW basis_grundf_vol_yx_pro_plotobs_totholz AS
 SELECT plot_obs.id,
    plot_obs.plot_id,
    plot_obs.id AS plot_obs_id,
    plot_obs.year,
    plot_obs.municipality_id,
    COALESCE(subq."Anzahl Probebaeume", 0) AS "Anzahl Probebaeume",
    COALESCE(subq."Volumen m3", 0) AS "Volumen m3",
    COALESCE(subq."Grundflaeche m2", 0) AS "Grundflaeche m2",
    COALESCE(subq."Anzahl Probebaeume"::numeric * ksp_lokale_dichte(plot_obs.forest_edgef), 0) AS "Stammzahl pro ha",
    COALESCE(subq."Volumen m3" * ksp_lokale_dichte(plot_obs.forest_edgef), 0) AS "Volumen pro ha",
    COALESCE(subq."Grundflaeche m2" * ksp_lokale_dichte(plot_obs.forest_edgef)::double precision, 0) AS "Grundflaeche pro ha",
    plot_obs.forest_edgef
   FROM ( SELECT tree_obs.obs_id,
            count(*) AS "Anzahl Probebaeume",
            sum((ksp_volume_et_bl(tree_obs.dbh))*lt_stem.dead_wood_volume) AS "Volumen m3",
            sum(ksp_grundflaeche_bl(tree_obs.dbh)) AS "Grundflaeche m2"
           FROM tree_obs
             JOIN tree ON tree_obs.tree_id = tree.id
             JOIN lt_stem ON tree_obs.stem_id = lt_stem.id
          WHERE tree_obs.vita_id = 4
          GROUP BY tree_obs.obs_id) subq
     FULL JOIN plot_obs ON subq.obs_id = plot_obs.id;""",
            "DROP VIEW basis_grundf_vol_yx_pro_plotobs_totholz"
        ),

        migrations.CreateModel(
            name='Zuwachs',
            fields=[
                ('id', models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='observation.PlotObs')),
                ('plot', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='observation.Plot')),
                ('leb_stem', models.IntegerField(verbose_name='Lebende Stammzahl')),
                ('leb_growth_ha', models.FloatField(verbose_name='Lebender Zuwachs/ha/Jahr')),
                ('ein_stem', models.IntegerField(verbose_name='Einwuchs Stammzahl')),
                ('ein_growth_ha', models.FloatField(verbose_name='Einwuchs Zuwachs/ha/Jahr')),
                ('mor_stem', models.IntegerField(verbose_name='Tote Stammzahl')),
                ('mor_growth_ha', models.FloatField(verbose_name='Tote/genutzte Bäume Zuwachs/ha/Jahr')),
                ('growth_ha_total', models.FloatField(verbose_name='Totaler Zuwachs/ha/Jahr')),
            ],
            options={
                'db_table': 'zuwachs_total',
                'managed': False,
            },
        ),
        migrations.RunSQL("""
 CREATE OR REPLACE VIEW basis_zuwachs AS
 SELECT tobsa.id,
    tobsa.tree_id,
    tobsa.obs_id,
    pob.inv_period AS invent0,
    poa.inv_period AS invent1,
    tobsb.dbh AS dbh0,
    tobsa.dbh AS dbh1,
    ksp_volume_et_bl(tobsb.dbh) AS vol0,
    ksp_volume_et_bl(tobsa.dbh) AS vol1,
    ksp_grundflaeche_bl(tobsb.dbh) AS grundf0,
    ksp_grundflaeche_bl(tobsa.dbh) AS grundf1,
    tobsa.dbh - tobsb.dbh AS dbh_diff,
    poa.year - pob.year AS year_diff,
    ksp_grundflaeche_bl(tobsa.dbh) - ksp_grundflaeche_bl(tobsb.dbh) AS grundf_diff,
    GREATEST(0::double precision, (ksp_volume_et_bl(tobsa.dbh) - ksp_volume_et_bl(tobsb.dbh))::double precision / (poa.year - pob.year)::double precision) AS growth_year
   FROM tree_obs tobsa
     JOIN tree_obs tobsb ON tobsa.tree_id = tobsb.tree_id
     LEFT JOIN plot_obs poa ON tobsa.obs_id = poa.id
     LEFT JOIN plot_obs pob ON tobsb.obs_id = pob.id
     LEFT JOIN lt_vita vitaa ON tobsa.vita_id = vitaa.id
     LEFT JOIN lt_vita vitab ON tobsb.vita_id = vitab.id
  WHERE vitaa.code::text = 'l'::text AND (vitab.code::text = ANY (ARRAY['e'::character varying::text, 'l'::character varying::text])) AND poa.inv_period = (pob.inv_period + 1);""",
            "DROP VIEW basis_zuwachs"
        ),
        migrations.RunSQL("""
CREATE OR REPLACE VIEW basis_zuwachs_pro_plotobs AS
 SELECT po.id,
    po.plot_id,
    sum(zuw.growth_year) AS growth,
    sum(zuw.growth_year) * ksp_lokale_dichte(po.forest_edgef)::double precision AS growth_ha,
    count(*) AS nb_stem,
    sum(zuw.growth_year) / count(*)::double precision * ksp_lokale_dichte(po.forest_edgef)::double precision AS growth_stem_ha
   FROM plot_obs po
     LEFT JOIN basis_zuwachs zuw ON zuw.obs_id = po.id
  GROUP BY po.id;""",
            "DROP VIEW basis_zuwachs_pro_plotobs"
        ),
        migrations.RunSQL("""
 CREATE OR REPLACE VIEW basis_zuwachs_einwuchs AS
 SELECT tobs.id,
    tobs.tree_id,
    tobs.obs_id,
    po_prev.inv_period AS invent0,
    po.inv_period AS invent1,
    tobs.dbh,
    tobs.dbh - 11 AS dbh_diff,
    po.year - po_prev.year AS year_diff,
    GREATEST(0::double precision, (ksp_volume_et_bl(tobs.dbh) - ksp_volume_et_bl(11::smallint))::double precision / (po.year - po_prev.year)::double precision / 2.0::double precision) AS growth_year
   FROM tree_obs tobs
     LEFT JOIN plot_obs po ON tobs.obs_id = po.id
     LEFT JOIN plot ON po.plot_id = plot.id
     LEFT JOIN plot_obs po_prev ON plot.id = po_prev.plot_id AND po.inv_period = (po_prev.inv_period + 1)
     LEFT JOIN lt_vita vita ON tobs.vita_id = vita.id
  WHERE vita.code::text = 'e'::text;""",
            "DROP VIEW basis_zuwachs_einwuchs"
        ),
        migrations.RunSQL("""
 CREATE OR REPLACE VIEW basis_zuwachs_einwuchs_pro_plotobs AS
 SELECT po.id,
    po.plot_id,
    COALESCE(sum(zuw.growth_year), 0::double precision) AS growth,
    COALESCE(sum(zuw.growth_year) * ksp_lokale_dichte(po.forest_edgef)::double precision, 0::double precision) AS growth_ha,
    count(zuw.obs_id) AS nb_stem,
    sum(zuw.growth_year) / count(*)::double precision * ksp_lokale_dichte(po.forest_edgef)::double precision AS growth_stem_ha
   FROM plot_obs po
     LEFT JOIN basis_zuwachs_einwuchs zuw ON zuw.obs_id = po.id
  GROUP BY po.id;""",
            "DROP VIEW basis_zuwachs_einwuchs_pro_plotobs"
        ),
        migrations.RunSQL("""
 CREATE OR REPLACE VIEW basis_stem_pro_plotobs_pro_vita AS
 SELECT tree_obs.obs_id AS id,
    count(*) AS total_stem,
    sum(
        CASE
            WHEN lt_vita.code::text = 'l'::text THEN 1
            ELSE 0
        END) AS lebend,
    sum(
        CASE
            WHEN lt_vita.code::text = 'e'::text THEN 1
            ELSE 0
        END) AS einwuchs,
    sum(
        CASE
            WHEN lt_vita.code::text = 'c'::text THEN 1
            ELSE 0
        END) AS nutzung,
    sum(
        CASE
            WHEN lt_vita.code::text = 'm'::text THEN 1
            ELSE 0
        END) AS tot
   FROM tree_obs
     LEFT JOIN plot_obs ON tree_obs.obs_id = plot_obs.id
     LEFT JOIN lt_vita ON tree_obs.vita_id = lt_vita.id
  GROUP BY tree_obs.obs_id;""",
            "DROP VIEW basis_stem_pro_plotobs_pro_vita"
        ),
        migrations.RunSQL("""
 CREATE OR REPLACE VIEW zuwachs_total AS
 SELECT subs.id,
    subs.plot_id,
    subs.leb_stem,
    subs.leb_perc,
    subs.leb_growth_ha,
    subs.ein_stem,
    subs.ein_growth_ha,
    subs.mor_stem,
    subs.mor_growth_ha,
        CASE
            WHEN subs.leb_perc >= 0.5::double precision OR subs.leb_stem >= 7 THEN subs.leb_growth_ha + subs.ein_growth_ha + subs.mor_growth_ha
            ELSE NULL::double precision
        END AS growth_ha_total
   FROM ( SELECT leb_zuwachs.id,
            leb_zuwachs.plot_id,
            stem_vita.lebend AS leb_stem,
                CASE
                    WHEN stem_vita.lebend = 0 THEN 0.0::double precision
                    ELSE stem_vita.lebend::double precision / (stem_vita.lebend + stem_vita.nutzung + stem_vita.tot)::double precision
                END AS leb_perc,
            leb_zuwachs.growth_ha AS leb_growth_ha,
            stem_vita.einwuchs AS ein_stem,
            ein_zuwachs.growth_ha AS ein_growth_ha,
            stem_vita.nutzung + stem_vita.tot AS mor_stem,
                CASE
                    WHEN (stem_vita.nutzung + stem_vita.tot) = 0 THEN 0::double precision
                    ELSE (stem_vita.nutzung + stem_vita.tot)::double precision * leb_zuwachs.growth_stem_ha / 2.0::double precision
                END AS mor_growth_ha
           FROM basis_zuwachs_pro_plotobs leb_zuwachs
             LEFT JOIN basis_zuwachs_einwuchs_pro_plotobs ein_zuwachs ON leb_zuwachs.id = ein_zuwachs.id
             LEFT JOIN basis_stem_pro_plotobs_pro_vita stem_vita ON leb_zuwachs.id = stem_vita.id) subs;""",
            "DROP VIEW zuwachs_total"
        ),
    ]
