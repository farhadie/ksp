from django.contrib import admin
from django.contrib.gis.db import models as gis_models
from django.contrib.gis.forms import OSMWidget

from . import models


class MyOSMWidget(OSMWidget):
    """ Overriden OSMWidget to set local JS files (collected with django-remote-finder). """
    class Media:
        extend = False
        css = {
            # Called 'relatively' from OpenLayers.js
            'all': ('js/theme/default/style.css',)
        }
        js = (
            'js/OpenLayers.js',
            'js/OpenStreetMap.js',
            'gis/js/OLMapWidget.js',
        )


class PlotAdmin(admin.ModelAdmin):
    list_display = ('nr', 'sealevel')
    search_fields = ('nr',)

class PlotObsAdmin(admin.ModelAdmin):
    list_display = ('plot', 'year', 'municipality')
    search_fields = ('plot__nr',)
    raw_id_fields = ('plot',)

class TreeSpeciesAdmin(admin.ModelAdmin):
    list_display = ('species', 'abbrev', 'is_tree')
    ordering = ('species',)

class TreeAdmin(admin.ModelAdmin):
    raw_id_fields = ('plot',)

class PhytosocAdmin(admin.ModelAdmin):
    search_fields = ('code', 'description')
    list_display = ('code', 'description')
    ordering = ('code',)

class TreeObsAdmin(admin.ModelAdmin):
    search_fields = ('id', 'obs__plot__nr',)
    raw_id_fields = ('obs', 'tree')

class RegenObsAdmin(admin.ModelAdmin):
    raw_id_fields = ('obs',)

class AdminRegionAdmin(admin.ModelAdmin):
    list_display = ('name', 'nr', 'region_type')
    list_filter = ('region_type',)
    ordering = ('name',)
    formfield_overrides = {
        gis_models.GeometryField: {'widget': MyOSMWidget},
    }


admin.site.register(models.ForestDomain)
admin.site.register(models.Owner)
admin.site.register(models.Phytosoc, PhytosocAdmin)
admin.site.register(models.Plot, PlotAdmin)
admin.site.register(models.OwnerType)
admin.site.register(models.Region)
admin.site.register(models.Sector)
admin.site.register(models.Gap)
admin.site.register(models.DevelStage)
admin.site.register(models.SoilCompaction)
admin.site.register(models.ForestForm)
admin.site.register(models.RegenType)
admin.site.register(models.ForestMixture)
admin.site.register(models.CrownClosure)
admin.site.register(models.StandStructure)
admin.site.register(models.Relief)
admin.site.register(models.Acidity)
admin.site.register(models.Geology)
admin.site.register(models.PlotObs, PlotObsAdmin)
admin.site.register(models.TreeSpecies, TreeSpeciesAdmin)
admin.site.register(models.Tree, TreeAdmin)
admin.site.register(models.RegenObs, RegenObsAdmin)
admin.site.register(models.SurveyType)
admin.site.register(models.Layer)
admin.site.register(models.Rank)
admin.site.register(models.Vita)
admin.site.register(models.Damage)
admin.site.register(models.DamageCause)
admin.site.register(models.CrownForm)
admin.site.register(models.CrownLength)
admin.site.register(models.Stem)
admin.site.register(models.TreeObs, TreeObsAdmin)
admin.site.register(models.UpdatedValue)
admin.site.register(models.RegionType)
admin.site.register(models.AdminRegion, AdminRegionAdmin)
