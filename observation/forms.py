from django import forms

from observation.models import UpdatedValue, Vita


class TreeReconcileForm(forms.Form):
    def __init__(self, tree1=None, tree2=None, *args, **kwargs):
        # tree1/tree2 ar TreeObs objects
        self.tree1 = tree1
        self.tree2 = tree2
        super(TreeReconcileForm, self).__init__(*args, **kwargs)

    def save(self):
        if self.tree1.tree.spec != self.tree2.tree.spec and self.tree2.tree.spec.species == "Totholz":
            # Replace Totholz from the second tree by the real species from the first tree
            new_spec = self.tree1.tree.spec
        else:
            new_spec = None
        old_tree = self.tree1.tree
        self.tree1.tree = self.tree2.tree
        self.tree1.save()
        msg = "Changed tree/species from %s to %s" % (old_tree.spec, self.tree2.tree.spec)
        UpdatedValue.add(self.tree1, 'tree_id', old_tree.pk, msg)
        if new_spec:
            tree = self.tree2.tree
            old_spec = tree.spec
            tree.spec = new_spec
            tree.save()
            msg = "Changed tree species from Totholz to %s" % (new_spec,)
            UpdatedValue.add(tree, 'spec_id', old_spec.pk, msg)
        if old_tree.treeobs_set.count() == 0:
            old_tree.delete()
        

class TreeEditForm(forms.Form):
    """ Combined TreeObs/Tree edit form """
    vita = forms.ModelChoiceField(queryset=Vita.objects.all())
    nr = forms.IntegerField()
    azimuth = forms.IntegerField()
    distance = forms.DecimalField()

    def __init__(self, *args, **kwargs):
        self.tree_obs = kwargs.pop('tree_obs')
        kwargs['initial'].update({
            'vita': self.tree_obs.vita,
            'nr': self.tree_obs.tree.nr,
            'azimuth': self.tree_obs.tree.azimuth,
            'distance': self.tree_obs.tree.distance,
        })
        super(TreeEditForm, self).__init__(*args, **kwargs)

    def save(self):
        tree_changed = False
        for key in self.changed_data:
            if key in {'nr', 'azimuth', 'distance'}:
                setattr(self.tree_obs.tree, key, self.cleaned_data[key])
                UpdatedValue.add(self.tree_obs.tree, key, self.initial[key], "")
                tree_changed = True
            if key == 'vita':
                self.tree_obs.vita = self.cleaned_data['vita']
                self.tree_obs.save()
                UpdatedValue.add(self.tree_obs, key, self.initial[key], "")
        if tree_changed:
            self.tree_obs.tree.save()
