from django.db.models import Aggregate
from django.db.models.expressions import Func


class LokaleDichte(Func):
    function = 'ksp_lokale_dichte'

    def __init__(self, *expressions, **extra):
        if len(expressions) != 1:
            raise ValueError('expressions must have exactly 1 element')
        super(LokaleDichte, self).__init__(*expressions, **extra)


class StdErrRel(Aggregate):
    """https://en.wikipedia.org/wiki/Standard_error#Relative_standard_error"""
    name = 'StdErrRel'
    template = 'stddev(%(expressions)s) / sqrt(count(*)) / nullif(avg(%(expressions)s), 0) * 100.0'
