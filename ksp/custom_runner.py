from django.test.runner import DiscoverRunner

class CustomRunner(DiscoverRunner):
    """
    This CutomRunner ensures that the 'afw' (unmanaged) database is not created
    by tests.
    """
    def setup_databases(self, **kwargs):
        from django.db import connections
        popped = None
        if 'afw' in connections._databases:
            popped = connections._databases.pop('afw')
        return super(CustomRunner, self).setup_databases(**kwargs)
        if popped:
            connections._databases['afw'] = popped
