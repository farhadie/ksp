# Django settings for ksp project.
import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

DEBUG = True

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'ksp2',
        'USER': '',
        'PASSWORD': '',
    },
    'afw': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'afw',
    }
}
DATABASE_ROUTERS = ['ksp.routers.DefaultRouter']

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Zurich'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'de'

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'remote_finder.RemoteFinder',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'ksp.middleware.LoginRequiredMiddleware',
)

ROOT_URLCONF = 'ksp.urls'

LOGIN_URL = '/login/'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'ksp.wsgi.application'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                # Default list
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
            # Uncomment on Django 1.9:
            #'builtins': ['django.contrib.staticfiles.templatetags.staticfiles'],
        },
    },
]

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.gis',
    'gemeinde',
    'imports',
    'observation',
    'document',
    'tariff',
)

REMOTE_FINDER_CACHE_DIR = os.path.join(BASE_DIR, 'remote-finder-cache')

REMOTE_FINDER_RESOURCES = [
    ('js/OpenLayers.js', 'http://openlayers.org/api/2.13.1/OpenLayers.js',
     'md5:50bd0248bb42ab83e6c9195111acdd68'),
    ('js/theme/default/style.css', 'http://openlayers.org/api/2.13.1/theme/default/style.css',
     'md5:d913ca3244cd2e59de875e1da15fa250'),
    ('js/theme/default/img/editing_tool_bar.png',
     'http://openlayers.org/api/2.13.1/theme/default/img/editing_tool_bar.png',
     'md5:326a1b7f698efc9d7853280e527c7f0f'),
    ('js/OpenStreetMap.js', 'http://www.openstreetmap.org/openlayers/OpenStreetMap.js',
     'md5:50775de3c4022d691fbbb828c7131cc3'),
    ('js/jquery-1.12.0.min.js', 'http://code.jquery.com/jquery-1.12.0.min.js',
     'md5:cbb11b58473b2d672f4ed53abbb67336'),
    ('js/jquery-ui.min.js', 'https://code.jquery.com/ui/1.11.4/jquery-ui.min.js',
     'md5:d935d506ae9c8dd9e0f96706fbb91f65'),

    ('css/themes/smoothness/jquery-ui.css',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css',
     'md5:64dfb75ef30cbf691e7858dc1992b4df'),
    ('css/themes/smoothness/images/ui-bg_flat_0_aaaaaa_40x100.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-bg_flat_0_aaaaaa_40x100.png',
     'md5:21f222d245c6229bf1086527335c49e8'),
    ('css/themes/smoothness/images/ui-bg_flat_75_ffffff_40x100.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-bg_flat_75_ffffff_40x100.png',
     'md5:985f46b74b703e605966201743fb6f7e'),
    ('css/themes/smoothness/images/ui-bg_highlight-soft_75_cccccc_1x100.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-bg_highlight-soft_75_cccccc_1x100.png',
     'md5:dd13068fa67603e4ed7302f69af8d905'),
    ('css/themes/smoothness/images/ui-bg_glass_65_ffffff_1x400.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-bg_glass_65_ffffff_1x400.png',
     'md5:177bd763ea7a4ac8fcb1dc94570dd833'),
    ('css/themes/smoothness/images/ui-bg_glass_75_dadada_1x400.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-bg_glass_75_dadada_1x400.png',
     'md5:2d6f73d4cc0d5c0e6bb8b05c2ebc1d11'),
    ('css/themes/smoothness/images/ui-bg_glass_75_e6e6e6_1x400.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-bg_glass_75_e6e6e6_1x400.png',
     'md5:5c579dd5f9969ffdc83a00007271684c'),
    ('css/themes/smoothness/images/ui-icons_888888_256x240.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-icons_888888_256x240.png',
     'md5:0979e30214877ac97c47cd1ea71a1058'),
    ('css/themes/smoothness/images/ui-icons_222222_256x240.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-icons_222222_256x240.png',
     'md5:5b5ec59318bb5f73baf58fcbfeca4e46'),
    ('css/themes/smoothness/images/ui-icons_454545_256x240.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-icons_454545_256x240.png',
     'md5:aa541bdd3a9d7e85aa84de5342c5712c'),

    ('js/jquery.cookie.js',
     'https://raw.githubusercontent.com/carhartl/jquery-cookie/master/src/jquery.cookie.js',
     'md5:0f1f6cd6e0036897019b376d38593403'),
    ('js/jquery.tablesorter.min.js',
     'https://raw.githubusercontent.com/christianbach/tablesorter/master/jquery.tablesorter.min.js',
     'md5:28f91818bc0e61a9b5445eed72e45ee5'),

    ('js/highlight.min.js',
     'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.3.0/highlight.min.js',
     'md5:e663cb8c64830ade4ae4795e150d29e3'),
    ('css/highlight.default.min.css',
     'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.3.0/styles/default.min.css',
     'md5:5133d11fbaf87d3978cf403eba33c764'),
]

TEST_RUNNER = 'ksp.custom_runner.CustomRunner'

from .local_settings import *
